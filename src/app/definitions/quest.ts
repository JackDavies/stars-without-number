import {Player} from './player';
import {Character} from './character';

export class Quest {
  guid: string;
  name: string;
  details: string;
  reward: number;
  owner: Character['guid'];
  complete: boolean;
}
