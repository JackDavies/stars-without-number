import {Character} from './character';
import {Faction} from './faction';

export class Loan {
  guid: string;
  title: string;
  debtor: Character['guid'] | Faction['guid'];
  creditor: Character['guid'] | Faction['guid'];
  amount: number;
  terms: string;
}
