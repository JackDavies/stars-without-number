import {Player} from './player';
import {Quest} from './quest';

export class Journal {
  guid: string;
  name: string;
  playerGUID: Player['guid'];
  quest: Array<Quest['guid']>;
  notes: Array<object>;
}
