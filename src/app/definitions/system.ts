import {Planet} from './planent';
import {Sector} from './sector';

export class System {
    guid: string;
    name: string;
    sector: Sector;
    hex: string;
    plants: Array<Planet>;
}
