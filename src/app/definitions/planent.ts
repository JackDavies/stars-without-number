import {System} from './system';
import {Sector} from './sector';
import {Faction} from './faction';

export class Planet {
    guid: string;
    name: string;
    sector: Sector;
    system: System;
    atmosphere: string;
    temperature: number;
    biosphere: string;
    population: number;
    techLevel: number;
    faction: Faction['guid'];
}
