export class Character {
  guid: string;
  name: string;
  class: string;
  level: number;
  gender: string;
  hitpoints: number;
  systemStrain: number;
  hitDie: number;
  background: string;
  skills: object;
  items: Array<object>;
  attributes: object;
  savingThrows: object;
  weapons: object;
  credits: number;
}
