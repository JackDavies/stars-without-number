import {Player} from './player';
import {Sector} from './sector';

export class Game {
  guid: string;
  title: string;
  players: Array<Player['guid']>;
  sector: Sector['guid'];
  currentDay: number;
}
