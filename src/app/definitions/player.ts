import {Character} from './character';
import {Journal} from './journal';
import {Faction} from './faction';

export class Player {
  guid: string;
  name: string;
  gmFlag: boolean;
  characters: Array<Character['guid']>;
  journal: Journal['guid'];
  factions: Array<Faction['guid']>;
}
