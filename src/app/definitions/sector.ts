import {System} from './system';

export class Sector {
    guid: string;
    name: string;
    hexMap: Array<System>;
}
