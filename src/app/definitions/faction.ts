import {Planet} from './planent';
import {FactionAsset} from './faction-asset';
import {FactionTag} from './faction-tag';

export class Faction {
  guid: string;
  name: string;
  hitpoints: number;
  force: number;
  cunning: number;
  weath: number;
  homeworld: Planet['guid'];
  goal: string;
  tags: Array<FactionTag>;
  assets: Array<FactionAsset>;
}
