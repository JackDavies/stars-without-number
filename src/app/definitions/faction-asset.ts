export class FactionAsset {
    guid: string;
    name: string;
    assetType: 'Force' | 'Cunning' | 'Weath';
    level: number;
    hitpoints: number;
    cost: number;
    techLevel: number;
    type: string;
    attack: string;
    counterAttack: string;
    special: string;
}
