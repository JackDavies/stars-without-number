import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';
import {Observable} from 'rxjs/Observable';

import {Journal} from '../../definitions/journal';
import {PlayerService} from '../player/player.service';

@Injectable({
  providedIn: 'root'
})
export class JournalsService {

  private _journals = {};
  public playerJournalsChange: Subject<object> = new Subject<object>();

  constructor(
    private http: HttpClient,
    private playerService: PlayerService
  ) {
    playerService.playerChange.subscribe(player => {
      this.init();
    });
    this.init();
  }

  private init() {
    if (this.playerService && this.playerService.player) {
      if (this.playerService.player.gmFlag) {
        // Load all journals;
      } else if (this.playerService.player.journals) {
        this._journals = {};
        this.playerService.player.journals.forEach( journalGUID => {
          this.loadJournal(journalGUID).subscribe( journal => {
            this._journals[journalGUID] = journal;
            this.playerJournalsChange.next(this._journals);

          });

        });

      }
    }
  }

  private loadJournal(journalGUID) {
    console.log(journalGUID)
    return new Observable((observer) => {
      this.http.get('database/journals/' + journalGUID + '.json').subscribe( (journal: Journal) => {
        if (journal) {
          observer.next(journal);
        } else {
          // error handling
        }
      }, err => {
        // error handling
      });

    });
  }

  get journals() {
    return this._journals;
  }
}
