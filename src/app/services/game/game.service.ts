import {Injectable} from '@angular/core';
import {PlayerService} from '../player/player.service';
import {Subject} from 'rxjs';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {Game} from '../../definitions/game';

@Injectable({
  providedIn: 'root'
})

export class GameService {

  private _game;
  public gameChange: Subject<Game> = new Subject<Game>();

  constructor(
    private http: HttpClient,
    private playerService: PlayerService
  ) {
    this.init();
    this.playerService.playerChange.subscribe(player => {
      this.init();
    });
  }

  private init() {
    if (this.playerService && this.playerService.player) {
      this.loadGame(this.playerService.player.game).subscribe(  (game: Game) => {
        this.gameChange.next(game);
      });
    }
  }

  private loadGame(gameGUID): Observable<Game> {
    return new Observable((observer) => {
      this.http.get('database/games/' + gameGUID + '.json').subscribe( (game: Game) => {
        if (game) {
          observer.next(game[gameGUID]);
        } else {
          // error handling
        }
      }, err => {
        // error handling
      });

    });
  }

  get game() {
    return this._game;
  }
}
