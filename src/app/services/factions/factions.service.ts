import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PlayerService} from '../player/player.service';
import {Subject} from 'rxjs';
import {Player} from '../../definitions/player';
import {Faction} from '../../definitions/faction';

@Injectable({
  providedIn: 'root'
})
export class FactionsService {

  private _factions = [];
  public factionsChange: Subject<object> = new Subject<object>();

  constructor(
    private http: HttpClient,
    private playerService: PlayerService
  ) {
    this.factionsChange.subscribe( (next: Array<Faction['guid']>) => {
      console.log(next);
      this._factions = next;
    });

    if (this.playerService && this.playerService.player) {
      this.factionsChange.next(this.playerService.player.factions);
    }
    this.playerService.playerChange.subscribe((player: Player) => {
      this.factionsChange.next(player.factions);
    });
  }

  get factions() {
    return this._factions;
  }
}
