import {Injectable} from '@angular/core';

import {UtilsService} from '../utils/utils.service';
import {SkillsService} from '../skills/skills.service';

@Injectable({
  providedIn: 'root'
})
export class LevelUpService {

  constructor(
    private UtilsService:UtilsService,
    private SkillsService:SkillsService
  ) { }

  leveUp(npc){
    npc.level++;
    this.hitPoints(npc);
    this.skills(npc);
    this.credits(npc);
  }

  hitPoints(npc){
    let currentHp = npc.hitpoints;
    console.log(npc)
    let newHp = this.UtilsService.rollDice((npc.level + 1), npc.hitDie) + (npc.level * npc.attributes.constitution)
    if (newHp > currentHp){

      npc.hitpoints = newHp;
    }

  }

  skills(npc){
    let skillsArray = [];

    for (let key in npc.skills) {
      if (npc.skills.hasOwnProperty(key) && npc.skills[key].classSkill) {
        skillsArray.push(npc.skills[key])
      }
    }


    //shuffle the array so do not always level up first defined;
    this.shuffle(skillsArray);

    skillsArray.sort(function(a, b){return b.level - a.level});//high -> low
    skillsArray = skillsArray.slice(0,3);
    let highestLevel = skillsArray[0].level;

    skillsArray.sort(function(a, b){
      return a.level - b.level
    });//low -> high
    let lowestLevel = skillsArray[0].level;

    if(lowestLevel == highestLevel){
      skillsArray.sort(function(a, b){
        return b.skillPoints - a.skillPoints
      });
    }

    skillsArray[0].skillPoints += 3;
    skillsArray[0].level = this.SkillsService.levelCalc(skillsArray[0].skillPoints, true);
  }

  credits(npc){
    npc.credits = this.UtilsService.rollDice(npc.level + 1, 20) * 100
  }

  shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
      // Pick a random index
      let index = Math.floor(Math.random() * counter);

      // Decrease counter by 1
      counter--;

      // And swap the last element with it
      let temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
    }

    return array;
  }

}
