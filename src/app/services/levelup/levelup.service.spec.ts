import {TestBed, inject} from '@angular/core/testing';

import {LevelUpService} from './levelup.service';

describe('LevelUpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LevelUpService]
    });
  });

  it('should be created', inject([LevelUpService], (service: LevelUpService) => {
    expect(service).toBeTruthy();
  }));
});
