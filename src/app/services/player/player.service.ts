import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../auth/auth.service';
import {Subject} from 'rxjs';
import {Player} from '../../definitions/player';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  private players;
  private _player;
  public playerChange: Subject<Player> = new Subject<Player>();

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {
    http.get('database/players.json').subscribe( players => {
      this.players = players;
      if (authService.playerGUID) {
        this.playerChange.next(players[authService.playerGUID]);
      }
    });

    authService.playerGUIDChange.subscribe(guid => {
      if (this.players) {
        this.playerChange.next(this.players[guid]);
      }
    });

    this.playerChange.subscribe(player => {
      this._player = player;
    });


  }

  get player() {
    return this._player;
  }
}
