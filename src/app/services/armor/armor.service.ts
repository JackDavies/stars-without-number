import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ArmorService {

  private _armor:{
    'shieldT0':{
      name:'Shield - TL:0',
      ac: -1,
      encumbrance:1,
      cost:10,
      techLevel: 0
    },
    'shieldT1':{
      name:'Shield - TL:1',
      ac: -1,
      encumbrance:1,
      cost:20,
      techLevel: 1
    },
    'leatherJacket':{
      name:'Leather Jacket',
      ac: 7,
      encumbrance:1,
      cost:10,
      techLevel: 0
    }
  };
  constructor() { }

  get armor() {
    return this._armor;
  }
}
