import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SkillsService {
  private _skills = {
    artist:{
      name:'Artist',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'The character is trained or gifted in a particular type of art. The character should select a specific medium when this skill is gained.'
    },
    athletics:{
      name:'Athletics',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'Running, jumping, climbing, swimming, and other feats of athletics or acrobatics are covered by this skill.'
    },
    bureaucracy:{
      name:'Bureaucracy',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'A skilled bureaucrat knows how to deal with complex legal and administrative systems, and how to fi nd fi les or records quickly.'
    },
    business:{
      name:'Business',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'The character knows how to run a business and deal with other entrepreneurs.'
    },
    combatEnergy:{
      name:'Combat/Energy',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'Use of high-tech weaponry that relies on energy emissions such as lasers or plasma.'
    },
    combatGunnery:{
      name:'Combat/Gunnery',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'Use of heavy vehicle-mounted weapons or fi xed guns, whether on spacecraft or planetary vehicles.'
    },
    combatPrimitive:{
      name:'Combat/Primitive',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'Use of muscle-powered weapons such as knives, bows, or clubs, as well as thrown weapons such as grenades. This includes high-tech versions of these weapons.'
    },
    combatProjectile:{
      name:'Combat/Projectile',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'Use of mechanically-powered weaponry such as crossbows, pistols, rifl es, and the like.'
    },
    combatPsitech:{
      name:'Combat/Psitech',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'Use of psitech weaponry. This skill is usually useless to non-psychics, as almost all psitech weaponry requires some psionic talent to use.'
    },
    combatUnarmed:{
      name:'Combat/Unarmed',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'Weaponless combat techniques. Unlike other forms of weaponry, a practitioner of unarmed combat can add their Combat/' +
      'Unarmed skill to the damage roll as well as the hit roll. Characters with Unarmed-2 expertise are so good that they can even injure targets in' +
      'powered armor and other gear that would otherwise prevent effective Unarmed attacks.'
    },
    computer:{
      name:'Computer',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'The character is skilled at operating and programming computers, as well as operating most advanced communications systems.'
    },
    cultureAlien:{
      name:'Culture/Alien',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'The character is familiar with a particular alien race, knowing their traditions, physiology, and psychology. They can also speak and' +
      'understand the aliens’ language, assuming it is physically possible for a human to do so.'
    },
    cultureCriminal:{
      name:'Culture/Criminal',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'The character is familiar with criminal subcultures and the protocols for dealing with black markets and underground organizations.'
    },
    cultureSpacer:{
      name:'Culture/Spacer',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'The character knows the traditions and customs of interstellar spacefarers and deep-space colonists. They are comfortable in zero-g' +
      'environments, and can identify ships and astronautic equipment.'
    },
    cultureTraveller:{
      name:'Culture/Traveller',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'This skill is unique in that it can only be taken at level 0 and cannot be raised. It can substitute for any other planet’s Culture skill,' +
      'however, and represents a casual, basic knowledge of many different worlds. This skill is useless on worlds that have been completely' +
      'cut off from interstellar contact. Traveller skill grants no linguistic proficiency.'
    },
    cultureWorld:{
      name:'Culture/World',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'This specialty must be taken individually for each specific world, and relates to knowledge of that world’s society, government,' +
      'tech level, and laws. Level 0 in this skill also grants basic proficiency in that world’s most common language if the character is not already proficient in it.'
    },
    exosuit:{
      name:'Exosuit',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'The character is trained in the use of powered exosuits. These forms of heavy armor are often used in dangerous environments ranging from' +
      'asteroid mining to shock trooper assaults on enemy positions.'
    },
    gambling:{
      name:'Gambling',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'The character knows numerous games of chance and has a better than usual chance of winning them.'
    },
    history:{
      name:'History',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'The character has training in galactic history and can identify worlds, events, and people of historical importance.'
    },
    instructor:{
      name:'Instructor',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'The character knows how to teach the skills they possess. A psychic must have at least level 3 in this skill in order to safely train an' +
      'unskilled psychic, and must have received the special instruction required for a psionic mentor.'
    },
    language:{
      name:'Language',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'The character speaks additional languages. At each level, the character learns a number of new languages equal to the skill’s level plus one.'+
      'Thus, at level 0, they learn one new language, at level 1, they learn two new languages, and so forth.'
    },
    leadership:{
      name:'Leadership',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:'The character is trained in leading others in high-stress situations. Characters with this skill can keep the obedience of subordinates even'+
      'in dangerous situations or when giving hazardous orders.'
    },
    navigation:{
      name:'Navigation',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    perception:{
      name:'Perception',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    persuade:{
      name:'Persuade',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    profession:{
      name:'Profession',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    religion:{
      name:'Religion',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    science:{
      name:'Science',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    security:{
      name:'Security',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    stealth:{
      name:'Stealth',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    steward:{
      name:'Steward',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    survival:{
      name:'Survival',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    tactics:{
      name:'Tactics',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    techAstronautic:{
      name:'Tech/Astronautic',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    techMaltech:{
      name:'Tech/Maltech',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    techMedical:{
      name:'Tech/Medical',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    techPostech:{
      name:'Tech/Postech',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    techPretech:{
      name:'Tech/Pretech',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    techPsitech:{
      name:'Tech/Psitech',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    vehicleAir:{
      name:'Vehicle/Air',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    vehicleGrav:{
      name:'Vehicle/Grav',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    vehicleLand:{
      name:'Vehicle/Land',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    vehicleSpace:{
      name:'Vehicle/Space',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    },
    vehicleWater:{
      name:'Vehicle/Water',
      level:-1,
      skillPoints:0,
      classSkill:false,
      description:''
    }};

  private skillLevels = {
    0:{
      minChar:1,
      costClass:1,
      costOther:2,
      trainerFee:500,
    },
    1:{
      minChar:1,
      costClass:2,
      costOther:3,
      trainerFee:1000,
    },
    2:{
      minChar:3,
      costClass:3,
      costOther:4,
      trainerFee:2000,
    },
    3:{
      minChar:6,
      costClass:4,
      costOther:5,
      trainerFee:4000,
    },
    4:{
      minChar:9,
      costClass:5,
      costOther:6,
      trainerFee:8000,
    },
    5:{
      minChar:12,
      costClass:6,
      costOther:7,
      trainerFee:16000,
    },
    6:{
      minChar:15,
      costClass:7,
      costOther:8,
      trainerFee:32000,
    }
  };
  constructor() { }

  get skills(){
    for (let key in this._skills) {
      if (this._skills.hasOwnProperty(key)) {
        this._skills[key].skill = key;
      }
    }
    return this._skills;
  }

  getSkillsByName(skillsNameArray){
    let filteredSkills = [];
    let skills = [];
    for (let entry in this.skills) {
      skills.push(entry)
    }

    for(let i = 0; i < skills.length; i++){
      let selectedSkill = skills[i];
      for(let n = 0; n < skillsNameArray.length; n++){
        let filteringSkillName = skillsNameArray[n];
        if(selectedSkill.indexOf(filteringSkillName) >= 0){
          filteredSkills.push(selectedSkill)
        }
      }
    }
    return filteredSkills
  }

  getFilteredSkill(bannedSkills, numberOfSkills){
    let filteredSkills = [];
    let skills = [];
    for (let entry in this.skills) {
      skills.push(entry)
    }

    var limit = numberOfSkills ? numberOfSkills++ : skills.length;
    //while the number of desired skills hasn't been reach, or we have not looked at every item in skills.
    for(let i = 0; i < limit && skills.length; i++){
      let selectedSkill = skills.splice((Math.floor(Math.random() * skills.length)),1)[0];

      let banned = false;
      for(let n = 0; n < bannedSkills.length; n++){
        if(selectedSkill.indexOf(bannedSkills[n].replace('*','')) >= 0){
          banned = true;
        }
      }

      if(banned){
        i--
      } else {
        filteredSkills.push(selectedSkill)
      }

    }
    return filteredSkills.sort()
  }

  costCalc(level, classSkill){
    let i = 0;
    let cost = 0;
    while (i <= level){
      cost += i+(classSkill?1:2);
      ++i;
    }
    return cost;
  }

  levelCalc(skillPoints, classSkill){
    let level = -1;
    let levelCost = 0;

    while (levelCost <=  skillPoints){
      levelCost = this.costCalc(level, classSkill);

      if(levelCost <=  skillPoints){
        level++
      } else {
        //Cost to much return last level
        level = level - 1
      }
    }
    return level;
  }

}
