import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';
import {Observable} from 'rxjs/Observable';

import {Character} from '../../definitions/character';
import {PlayerService} from '../player/player.service';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  private _characters = {};
  private _playerCharacters;
  public playerCharactersChange: Subject<Array<Character>> = new Subject<Array<Character>>();

  constructor(
    private http: HttpClient,
    private playerService: PlayerService
  ) {
    playerService.playerChange.subscribe(player => {
      this.init();
    });
    this.init();
  }

  private init() {
    if (this.playerService && this.playerService.player) {
      if (this.playerService.player.gmFlag) {
        // Load all characters;
      } else if (this.playerService.player.characters) {
        this._playerCharacters = {};
        this.playerService.player.characters.forEach( characterGUID => {
          this.loadCharacter(characterGUID).subscribe( character => {
            this._playerCharacters[characterGUID] = character;
            this.playerCharactersChange.next(this._playerCharacters);

          });

        });

      }
    }
  }

  private loadCharacter(characterGUID) {
    return new Observable((observer) => {
      this.http.get('database/characters/' + characterGUID + '.json').subscribe( character => {
        if (character) {
          observer.next(character);
        } else {
          // error handling
        }
      }, err => {
        // error handling
      });

    });
  }

  get characters() {
    return this._playerCharacters;
  }
}
