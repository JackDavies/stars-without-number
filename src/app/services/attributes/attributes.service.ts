import {Injectable} from '@angular/core';

import {UtilsService} from '../utils/utils.service'

@Injectable({
  providedIn: 'root'
})
export class AttributesService {

  constructor(
    private UtilsService: UtilsService
  ) { }

  get attributes(){
    var modifier = function (roll) {
      if (roll >= 18){
        return 2
      } else if (roll >= 14){
        return 1
      }
      else if (roll >= 8){
        return 0
      }
      else if (roll >= 4){
        return -1
      } else {
        return -2
      }
    };
    return {
      strength:modifier(this.UtilsService.rollDice(3,6)),
      intelligence:modifier(this.UtilsService.rollDice(3,6)),
      wisdom:modifier(this.UtilsService.rollDice(3,6)),
      dexterity:modifier(this.UtilsService.rollDice(3,6)),
      constitution:modifier(this.UtilsService.rollDice(3,6)),
      charisma:modifier(this.UtilsService.rollDice(3,6)),
    }
  }
}
