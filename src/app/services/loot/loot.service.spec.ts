import {TestBed, inject} from '@angular/core/testing';

import {LootService} from './loot.service';

describe('LootService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LootService]
    });
  });

  it('should be created', inject([LootService], (service: LootService) => {
    expect(service).toBeTruthy();
  }));
});
