import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {nameGen} from "./nameGen";

@Injectable({
  providedIn: 'root'
})
export class NameGenService {
  private names: Array<nameGen>;

  constructor(
    private http: HttpClient
  ) {
    this.getNames();
  }

  get nameAndSex(): object {
    const obj = this.names.splice(Math.floor(Math.random() * this.names.length), 1)[0];
    const name = obj.name + ' ' + obj.surname;
    const gender = obj.gender;

    localStorage.setItem('namesArray', JSON.stringify(this.names));
    if (!this.names || this.names.length <= 0) {
      this.getNames();
    }
    return {name : name, gender: gender};
  }

  private getNames(){
    const namesArray = JSON.parse(localStorage.getItem('namesArray'));

    if (!namesArray || namesArray.length <= 0) {
      const url = 'https://uinames.com/api/?amount=100';


      this.http.get(url).subscribe((res : Array<nameGen>) => {
        localStorage.setItem('namesArray', JSON.stringify(res));
        this.names = res;
      });
    } else {
      this.names = namesArray
    }

  }
}
