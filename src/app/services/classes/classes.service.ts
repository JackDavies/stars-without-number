import {Injectable} from '@angular/core';

import {SkillsService} from '../skills/skills.service'

@Injectable({
  providedIn: 'root'
})
export class ClassesService {
  private expertClasses = this.SkillsService.getFilteredSkill(['combat*', 'tatcis', 'techPsitech'], null);
  private psychicClasses = this.skillGen(['combatPsitech', 'culture*', 'history', 'perception', 'profession*', 'religion', ' techMedical', 'techPsitech']);
  private warriorClasses = this.skillGen(['Athletics', 'combat*', 'culture*', 'exosuit', 'leadership', 'perception', 'profession', 'stealth', 'survival', 'tactics']);

  private _classes = {
    Expert:{
      type:'Expert',
      hitDie:6,
      skillPointsPerLevel:3,
      special:'Experts have a knack for success, and can re-roll a failed skill check once per hour.',
      skills: this.expertClasses,
      throws: {
        4:{
          ab:0,
          physical:16,
          metal:15,
          evasion:12,
          tech:11,
          luck:14
        },
        // 8:{
        //   ab:2,
        //   physical:14,
        //   metal:13,
        //   evasion:10,
        //   tech:9,
        //   luck:12
        // },
        // 12:{
        //   ab:4,
        //   physical:12,
        //   metal:11,
        //   evasion:8,
        //   tech:7,
        //   luck:10
        // },
        // 16:{
        //   ab:6,
        //   physical:10,
        //   metal:9,
        //   evasion:6,
        //   tech:4,
        //   luck:7
        // },
        // 20:{
        //   ab:8,
        //   physical:9,
        //   metal:8,
        //   evasion:6,
        //   tech:4,
        //   luck:7
        // }
      },
      trainingPackages:[
        {
          'package':'Adventuring Expert',
          skills:this.expertAdvenutreSkillsGen(),
        },
        {
          'package':'Bounty Hunter',
          skills:this.skillGen(['combat*', 'culture*', 'navigation', 'persuade', 'stealth', 'survival', 'tactics', 'vehicle*']),
        }
      ]
    },
    Psychic:{
      type:'Psychic',
      hitDie:4,
      skillPointsPerLevel:2,
      psychicPointsPerLevel:2,
      PP:0,
      special:'Only psychics can learn psionic powers.',
      skills: this.psychicClasses,
      throws : {
        5:{
          ab:0,
          physical:13,
          metal:12,
          evasion:15,
          tech:16,
          luck:14
        },
      },
      trainingPackages:[
        {
          'package':'Adventuring Expert',
          skills:this.psychicAdvenutreSkillsGen(),
        }
      ]
    },
    Warrior:{
      type:'Warrior',
      hitDie:8,
      skillPointsPerLevel:2,
      psychicPointsPerLevel:0,
      special:'Warriors have uncanny luck on the battlefield. Once per fi ght, they can automatically evade an attack that would otherwise have hit them.',
      skills: this.warriorClasses,
      throws: {
        3:{
          ab:1,
          physical:12,
          metal:15,
          evasion:14,
          tech:16,
          luck:13
        },
        // 6:{
        //   ab:3,
        //   physical:10,
        //   metal:13,
        //   evasion:12,
        //   tech:14,
        //   luck:11
        // },
        // 9:{
        //   ab:5,
        //   physical:8,
        //   metal:11,
        //   evasion:10,
        //   tech:12,
        //   luck:9
        // },
        // 12:{
        //   ab:7,
        //   physical:6,
        //   metal:9,
        //   evasion:8,
        //   tech:10,
        //   luck:7
        // }
      },
      trainingPackages:[
        {
          'package':'Adventuring Warrior',
          skills:this.warriorAdvenutreSkillsGen(),
        },
        {
          'package':'Assassin',
          skills:this.skillGen(['athletics','combat*', 'cultureCriminal', 'Security', 'stealth', 'tactics']),
        }
      ]
    }};

  constructor(
    private SkillsService:SkillsService
  ){}

  public getClass(classType){
    let selectedClass;
    if (classType){
      selectedClass = this._classes[classType];
    } else {
      let i = Object.keys(this._classes);
      classType = i[Math.floor(Math.random() * i.length)]
      selectedClass = this._classes[classType];
    }

    selectedClass.trainingPackage = selectedClass.trainingPackages[Math.floor(Math.random() * selectedClass.trainingPackages.length)]
    return selectedClass;
  }

  get classList() {
    return Object.keys(this._classes);
  }

  expertAdvenutreSkillsGen(){
    //Create filter of non aloud, remove 2 that can be aloud.
    let classSetSkills = this.expertClasses;
    let selectedClassSkills = [];
    for(let i = 0; i < 4; i++){
      selectedClassSkills.push( classSetSkills.splice((Math.floor(Math.random() * classSetSkills.length)),1)[0])
    }
    const randomSkills =  this.SkillsService.getFilteredSkill(this.psychicClasses, 2);
    const decidedSKills = randomSkills.concat(selectedClassSkills)
    return decidedSKills
  }

  psychicAdvenutreSkillsGen(){
    let classSetSkills = this.psychicClasses;
    let selectedClassSkills = [];
    for(let i = 0; i < 1; i++){
      selectedClassSkills.push( classSetSkills.splice((Math.floor(Math.random() * classSetSkills.length)),1)[0])
    }
    const randomSkills =  this.SkillsService.getFilteredSkill(this.psychicClasses, 1);
    const decidedSKills = randomSkills.concat(selectedClassSkills)
    return decidedSKills
  }

  warriorAdvenutreSkillsGen(){
    //Create filter of non aloud, remove 2 that can be aloud.
    let classSetSkills = this.warriorClasses;
    let selectedClassSkills = [];
    for(let i = 0; i < 2; i++){
      selectedClassSkills.push( classSetSkills.splice((Math.floor(Math.random() * classSetSkills.length)),1)[0])
    }
    const randomSkills =  this.SkillsService.getFilteredSkill(this.warriorClasses, 2);
    const decidedSKills = randomSkills.concat(selectedClassSkills)
    return decidedSKills
  }

  skillGen(skillFilter){
    let selectedSKills = [];
    let possibleSKills = this.SkillsService.getSkillsByName(skillFilter);
    var loops = possibleSKills.length;
    for(let i = 0; i < loops; i++){

      let possibleSkill = possibleSKills.splice((Math.floor(Math.random() * possibleSKills.length)),1)[0]
      for(let n = 0; n < skillFilter.length; n++){
        if(possibleSkill.indexOf(skillFilter[n]) > -1){
          skillFilter.splice(n,1);
          selectedSKills.push(possibleSkill);
        }
      }
    }
    return selectedSKills.sort()
  }
}
