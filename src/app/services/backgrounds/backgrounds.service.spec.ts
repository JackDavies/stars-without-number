import {TestBed, inject} from '@angular/core/testing';

import {BackgroundsService} from './backgrounds.service';

describe('BackgroundsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BackgroundsService]
    });
  });

  it('should be created', inject([BackgroundsService], (service: BackgroundsService) => {
    expect(service).toBeTruthy();
  }));
});
