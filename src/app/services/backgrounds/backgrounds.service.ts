import {Injectable} from '@angular/core';

import {SkillsService} from '../skills/skills.service';

@Injectable({
  providedIn: 'root'
})
export class BackgroundsService {
  private _backgrounds = {
    adventurer:{
      background:'Adventurer',
      skills: this.adventureSkillsGen(),
      description:'Space is vast, and there are always those who refuse to fi t a particular\n' +
      'mold. This background choice is for those who would prefer to\n' +
      'pick their own skills and make their own past.'
    },
    armsman:{
      background:'Armsman',
      skills:['combatPrimitive', 'combatUnarmed', 'cultureWorld', 'tactics'],
      description:'Lostworlders often build their cultures on brute force and cold\n' +
      'steel. These men and women are often in the pay of a local warlord\n' +
      'or religious leader, using hard-won combat skills and a keen tactical\n' +
      'eye to do their master’s bidding. Some of them who end up on the\n' +
      'wrong side of a political question have reason to get off world, and\n' +
      'quickly.'

    },
    astrogatorsMate:{
      background:"Astrogator's Mate",
      skills:['cultureSpacer', 'navigation', 'science', 'vehicleSpace'],
      description: 'Scavenger Fleets and major spacecraft have an entire crew of\n' +
      'astrogators dedicated to plotting the next spike drive path and\n' +
      'navigating the dangerous energy fl uxes of the space between. These\n' +
      'men and women require further seasoning before they’d be trusted\n' +
      'with a post as a full astrogator, but they still learn the essentials of\n' +
      'the profession.'
    }};
  constructor(
    private SkillsService: SkillsService
  ) { }

  getBackground(backgroundType) {
    let i = Object.keys(this._backgrounds);
    backgroundType = backgroundType ? backgroundType : i[Math.floor(Math.random() * i.length)];
    return this._backgrounds[backgroundType];
  }

  get backgroundList() {
    let i = Object.keys(this._backgrounds);
    return i;
  }

  adventureSkillsGen(){
    let setSkills = ['cultureWorld', 'cultureSpacer'];
    let others = this.SkillsService.getFilteredSkill(setSkills, 2)
    return setSkills.concat(others);
  }
}
