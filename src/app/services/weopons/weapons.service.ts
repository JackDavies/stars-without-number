import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WeaponsService {
  private _weapons = {
    primitive:{
      knife:{
        name:'Knife',
        type:'Primitive',
        damage:'1d4',
        range:'6/9',
        cost: 5,
        attributes:['strength', 'dexterity'],
        techLevel: 0,
      },
      club:{
        name:'Club',
        type:'Primitive',
        damage:'1d4',
        range:'-',
        cost: 0,
        attributes:['strength', 'dexterity'],
        techLevel: 0,
      },
      primitiveBow:{
        name:'Primitive Bow',
        type:'Primitive',
        damage:'1d6',
        range:'50/75',
        cost: 15,
        attributes:['dexterity'],
        techLevel: 1,
      },
      stunBaton: {
        name:'Stun Baton',
        type:'Primitive',
        damage:'1d8*  Damage done by a stun baton can leave a target unconscious, but will not kill them.',
        range:'-',
        cost: 50,
        attributes:['strength'],
        techLevel: 3,
      },
      grenade: {
        name:'Grenade',
        type:'Primitive',
        damage:'2d8',
        range:'10/30',
        cost: 25,
        attributes:['strength', 'dexterity'],
        techLevel: 3,
      }

    },
    projectileWeaponry:{
       crudePistol:{
        name:'Crude Pistol',
        type:'Projectile',
        damage:'1d6',
        range:'5/15',
        cost: 20,
        magazine:'1@',
        attributes:['dexterity'],
        techLevel: 2,
      },
      Revolver:{
        name:'Revolver',
        type:'Projectile',
        damage:'1d8',
        range:'30/100',
        cost: 50,
        magazine:6,
        attributes:['dexterity'],
        techLevel: 2,
      },
      Shotgun:{
        name:'Shotgun',
        type:'Projectile',
        damage:'3d4',
        range:'10/30',
        cost: 50,
        magazine:2,
        attributes:['dexterity'],
        techLevel: 2,
      },
      semiAutomaticPistol:{
        name:'Semi-automatic Pistol',
        type:'Projectile',
        damage:'1d6 + 1',
        range:'30/100',
        cost: 75,
        magazine:12,
        attributes:['dexterity'],
        techLevel: 3,
      }
    },
    energyWeaponry:{
      laserPistol:{
        name:'Laser Pistol',
        type:'Energy',
        damage:'1d6',
        range:'100/300',
        cost: 200,
        magazine:10,
        attributes:['dexterity'],
        techLevel: 4,
      }
    }

  };

  constructor() { }

  get weapons() {
    return this._weapons;
  }

  randomWeapon(minLevel, maxLevel) {
    let i = Object.keys(this._weapons);
    let classType = i[Math.floor(Math.random() * i.length)]
    let selectedClass = this._weapons[classType];

    let n = Object.keys(selectedClass);
    let weopon = n[Math.floor(Math.random() * n.length)]
    let selectedweopon = selectedClass[weopon];

    return selectedweopon;
 
  }

}
