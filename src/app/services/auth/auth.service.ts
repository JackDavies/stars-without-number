import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {Router} from '@angular/router';
import {Player} from '../../definitions/player';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private _playerGUID;

  public playerGUIDChange: Subject<Player['guid']> = new Subject<Player['guid']>();

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    console.log(Math.floor((Math.random() * 1000000)));
    this._playerGUID = localStorage.getItem('playerGUID');

    this.playerGUIDChange.subscribe(guid => {
      localStorage.setItem('playerGUID', guid);
      this._playerGUID = guid;
    });
  }

  get playerGUID() {
    return this._playerGUID;
  }

  public signin(pn, pw) {
    return new Observable((observer) => {
      this.http.get('database/logins.json').subscribe( logins => {
        if (logins[pn] && logins[pn].password === pw) {
          this.playerGUIDChange.next(logins[pn].playerGUID);
          observer.next(true);
        } else {
          observer.next(false);
          return false;
        }
      }, err => {
        observer.next(false);
      });

    });
  }

  public logout() {
    this.playerGUIDChange.next(null);
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }

  public isAuthenticated() {
   return (this._playerGUID);
  }
}
