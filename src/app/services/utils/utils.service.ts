import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
  ) { }


  public rollDice(times,  dice) {
    let roll = 0;
    let i = 0;
    times = times ? times : 1;
    while (i < times){
      roll += Math.floor(Math.random() * dice + 1)
      i++;
    }
    return roll;
  }

  public objectKeys(obj): Array<string> {
    if (typeof obj === 'object') {
      return Object.keys(obj);
    }
    return [];
  }


  public generateUUID() {
    let d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
      d += performance.now();
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }
}
