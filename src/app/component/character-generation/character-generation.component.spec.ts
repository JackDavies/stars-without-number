import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CharacterGeneration} from './character-generation.component';

describe('CharacterGeneration', () => {
  let component: CharacterGeneration;
  let fixture: ComponentFixture<CharacterGeneration>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterGeneration ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterGeneration);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
