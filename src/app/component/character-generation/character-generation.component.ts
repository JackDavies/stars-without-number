import {Component} from '@angular/core';

import {Character} from '../../definitions/character';


import {AttributesService} from '../../services/attributes/attributes.service';
import {BackgroundsService} from '../../services/backgrounds/backgrounds.service';
import {ClassesService} from '../../services/classes/classes.service';
import {LevelUpService} from '../../services/levelup/levelup.service';
import {LootService} from '../../services/loot/loot.service';
import {SkillsService} from '../../services/skills/skills.service';
import {WeaponsService} from '../../services/weopons/weapons.service';
import {UtilsService} from '../../services/utils/utils.service';
import {NameGenService} from "../../services/nameGen/name-gen.service";

@Component({
  selector: 'character-gen',
  templateUrl: './character-generation.component.html',
  styleUrls: ['./character-generation.component.scss']
})
export class CharacterGeneration {


  public newNPC;

  public classList ;
  public selectedClass;
  public selectedLevel = 1;

  public backgroundList;
  public selectedBackground;

  public objectKeys = Object.keys;

  constructor(
    private attributesService: AttributesService,
    private backgroundsService: BackgroundsService,
    private classesService: ClassesService,
    private levelUpService: LevelUpService,
    private lootService: LootService,
    private skillsService: SkillsService,
    private weaponsService: WeaponsService,
    private utilsService: UtilsService,
    private nameGenService: NameGenService
  ) {
    this.classList = this.classesService.classList;
    this.backgroundList = this.backgroundsService.backgroundList;
  }



  generate() {
    // const utilsService = new UtilsService( );
    // const skillsService = new SkillsService();
    // this.ClassesService = new ClassesService(this.SkillsService);
    // this.BackgroundsService = new BackgroundsService(this.SkillsService);
    // this.AttributesService = new AttributesService(this.UtilsService);
    // this.WeaponsService = new WeaponsService();

    const character = new Character;
    const nameGen = this.nameGenService.nameAndSex;
    const npcClass = this.classesService.getClass(this.selectedClass);
    const background = this.backgroundsService.getBackground(this.selectedBackground);
    const attributes = this.attributesService.attributes;
    const skills = this.skillsService.skills;

    character.guid = this.utilsService.generateUUID();
    character.name = nameGen['name'];
    character.level = 1;
    character.hitpoints = this.utilsService.rollDice(1, npcClass['hitDie']) + (attributes.constitution);
    character.hitDie = npcClass['hitDie'];
    character.gender = nameGen['gender'];
    character.class = npcClass['type'];
    character.attributes = attributes;
    character.skills = skills;
    character.savingThrows = this.getSavingThrows(npcClass['throws']);

    character.weapons = this.weaponsService.randomWeapon(0, 6);
    character.credits = this.utilsService.rollDice(1, 100);


    this.markClassSKills(npcClass['skills']);
    this.setBaseSkillLevels(npcClass['trainingPackage']);
    this.setBaseSkillLevels(background['skills']);




    this.newNPC = character;
    for (let i = 1; i < this.selectedLevel; i++) {
      this.levelUp();
    }
    console.log(this.newNPC);
  }

  getSavingThrows(throws) {
    const keys = Object.keys(throws);
    // const selectedThrow;
    return throws[keys[0]];
  }


  markClassSKills(skillsArray){
    for (let i = 0; i < skillsArray.length; i++){
      const skillKey = skillsArray[i];
      this.skillsService.skills[skillKey].classSkill = true;
    }
  }

  setBaseSkillLevels(skillsArray) {
    for (let i = 0; i < skillsArray.length; i++){
      const skillKey = skillsArray[i];
      this.skillsService.skills[skillKey].level ++;
      this.skillsService.skills[skillKey].skillPoints = this.skillsService.costCalc(
        this.skillsService.skills[skillKey].level,
        this.skillsService.skills[skillKey].classSkill
      );
    }
  }

  levelUp() {
    this.levelUpService.leveUp(this.newNPC);
  }

  saveNew() {
    const npcs = JSON.parse(localStorage.getItem('npcs')) ? JSON.parse(localStorage.getItem('npcs')) : [];
    npcs.push(this.newNPC);
    localStorage.setItem('npcs', JSON.stringify(npcs));
  }

}
