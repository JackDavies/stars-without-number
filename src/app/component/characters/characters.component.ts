import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CharactersService} from '../../services/characters/characters.service';
import {UtilsService} from '../../services/utils/utils.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent {

  public characters;
  public characterGUID;

  constructor(
    private router: ActivatedRoute,
    private charactersService: CharactersService,
    public utils: UtilsService
  ) {
    this.router.params.subscribe( params => {
      this.characterGUID = params['guid'];
    });
    this.characters = charactersService.characters;
    charactersService.playerCharactersChange.subscribe(characters => {
      console.log(characters)
      this.characters = characters;
    });
  }
}
