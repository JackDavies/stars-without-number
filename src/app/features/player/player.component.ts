import {Component} from '@angular/core';
import {PlayerService} from '../../services/player/player.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent {

  public player;
  constructor(
    private playerService: PlayerService
  ) {
    this.player = playerService.player;
    playerService.playerChange.subscribe(player => {
      this.player = player;
    });
  }



}
