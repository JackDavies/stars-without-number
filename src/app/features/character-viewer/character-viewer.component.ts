import {Component, Input} from '@angular/core';
import {Character} from '../../definitions/character';
import {UtilsService} from '../../services/utils/utils.service';

@Component({
  selector: 'app-character-viewer',
  templateUrl: './character-viewer.component.html',
  styleUrls: ['./character-viewer.component.css']
})
export class CharacterViewerComponent {
  @Input() character: Character;
  constructor(
    public utils: UtilsService
  ) {

  }


  getAttackRoll(npc, weapon) {
    let highestBonus = -1;
    for (let i = 0; i < weapon.attributes.length; i++) {
      console.log(weapon.attributes[i])
      if (npc.attributes[weapon.attributes[i]] > highestBonus) {
        highestBonus = npc.attributes[weapon.attributes[i]];
      }
    }

    return '1d20 +' + npc['savingThrows']['ab'] + ' + ' + highestBonus + ' + ' + npc.skills['combat'+weapon.type].level;
  }

}
