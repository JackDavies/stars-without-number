import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {CharacterGeneration} from './component/character-generation/character-generation.component';
import {HomeComponent} from './pages/home/home.component';
import {LoginComponent} from './pages/login/login.component';
import {PlayerService} from './services/player/player.service';
import {PlayerComponent} from './features/player/player.component';
import {AuthService} from './services/auth/auth.service';
import {CharactersComponent} from './component/characters/characters.component';
import {AppRoutingModule} from './app-routing.module';
import {JournalsComponent} from './pages/journals/journals.component';
import {FactionsComponent} from './pages/factions/factions.component';
import {NpcComponent} from './pages/npc/npc.component';
import {CharacterViewerComponent} from './features/character-viewer/character-viewer.component';
import {GameService} from './services/game/game.service';
import { FactionViewerComponent } from './features/faction-viewer/faction-viewer.component';


@NgModule({
  declarations: [
    AppComponent,
    CharacterGeneration,
    HomeComponent,
    LoginComponent,
    PlayerComponent,
    CharactersComponent,
    JournalsComponent,
    FactionsComponent,
    NpcComponent,
    CharacterViewerComponent,
    FactionViewerComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    AuthService,
    PlayerService,
    GameService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
