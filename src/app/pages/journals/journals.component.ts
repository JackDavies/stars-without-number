import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UtilsService} from '../../services/utils/utils.service';
import {JournalsService} from '../../services/journals/journals.service';

@Component({
  selector: 'app-journals',
  templateUrl: './journals.component.html',
  styleUrls: ['./journals.component.css']
})
export class JournalsComponent implements OnInit {

  public journals;
  public journalGUID;

  constructor(
    private router: ActivatedRoute,
    private journalsService: JournalsService,
    public utils: UtilsService
  ) {
    this.router.params.subscribe( params => {
      this.journalGUID = params['guid'];
    });
    this.journals = journalsService.journals;
    journalsService.playerJournalsChange.subscribe(journals => {
      console.log(journals);
      this.journals = journals;
    });
  }

  ngOnInit() {
  }

}
