import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PlayerJournalComponent} from './player-journal.component';

describe('PlayerJournalComponent', () => {
  let component: PlayerJournalComponent;
  let fixture: ComponentFixture<PlayerJournalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerJournalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerJournalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
