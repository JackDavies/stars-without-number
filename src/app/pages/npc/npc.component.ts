import {Component} from '@angular/core';

import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-npc',
  templateUrl: './npc.component.html',
  styleUrls: ['./npc.component.css']
})
export class NpcComponent {
  public guid;
  public npcs;
  constructor(
    private activatedRoute: ActivatedRoute
  ) {
    this.npcs = JSON.parse(localStorage.getItem('npcs')) ? JSON.parse(localStorage.getItem('npcs')) : [];
   
    console.log(this.npcs);
    activatedRoute.params.subscribe(res => {
      console.log(res);
      if (res.guid) {
        this.guid = res.guid;
      }
    });
  }

  deleteNpc(index) {
    this.npcs.splice(index, 1);
    this.guid = null;
  }

  saveNpcs() {
    localStorage.setItem('npcs', JSON.stringify(this.npcs));
  }

  getSavingThrows(throws, level){
    const keys = Object.keys(throws);
    let selectedThrow;
    for (let i = 0; i < keys.length; i++){
      if (keys[i] > level ){
        selectedThrow = keys[i];
      }
    }
  }

  getAttackRoll(npc, weapon) {
    let highestBonus = -1;
    for (let i = 0; i < weapon.attributes.length; i++) {
      console.log(weapon.attributes[i])
      if (npc.attributes[weapon.attributes[i]] > highestBonus) {
        highestBonus = npc.attributes[weapon.attributes[i]];
      }
    }

    return '1d20 +' + npc['savingThrows']['ab'] + ' + ' + highestBonus + ' + ' + npc.skills['combat'+weapon.type].level;
  }
}
