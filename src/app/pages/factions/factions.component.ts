import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UtilsService} from '../../services/utils/utils.service';
import {FactionsService} from '../../services/factions/factions.service';

@Component({
  selector: 'app-factions',
  templateUrl: './factions.component.html',
  styleUrls: ['./factions.component.css']
})
export class FactionsComponent implements OnInit {

  public factions;
  public factionGUID;

  constructor(
    private router: ActivatedRoute,
    private factionsService: FactionsService,
    public utils: UtilsService
  ) {

  }

  ngOnInit() {
    this.router.params.subscribe( params => {
      this.factionGUID = params['guid'];
    });
    this.factions = this.factionsService.factions;
    this.factionsService.factionsChange.subscribe(factions => {
      console.log(factions);
      this.factions = factions;
    });
  }

}
