import {Component} from '@angular/core';
import {GameService} from '../../services/game/game.service';
import {FactionsService} from '../../services/factions/factions.service';
import {JournalsService} from '../../services/journals/journals.service';
import {CharactersService} from '../../services/characters/characters.service';
import {PlayerService} from '../../services/player/player.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  public player;

  public characters;
  public selectedCharacterGUID;

  public factions;
  public journals;
  constructor(
    private playerService: PlayerService,
    private charactersService: CharactersService,
    private factionsService: FactionsService,
    private journalsService: JournalsService,
  ) {

    this.player = playerService.player;
    playerService.playerChange.subscribe(player => {
      this.player = player;
      this.init();
    });

    this.characters = charactersService.characters;
    charactersService.playerCharactersChange.subscribe(characters => {
      this.characters = characters;
      this.init();
    });

    this.factions = factionsService.factions;
    factionsService.factionsChange.subscribe(factions => {
      this.factions = factions;
      this.init();
    });

    this.journals = journalsService.journals;
    journalsService.playerJournalsChange.subscribe(journals => {
      this.journals = journals;
      this.init();
    });
    this.init();
  }

  private init() {
    if (this.player && this.characters && this.factions && this.journals) {
      console.log(this.player);
      console.log(this.characters);

      this.selectedCharacterGUID = this.player.characters[0];

      console.log(this.factions);
      console.log(this.journals);
    }
  }



}
