import {Component} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  public error;
  public name: string;
  public password: string;

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  onSubmit() {
    this.auth.signin(this.name, this.password).subscribe( res => {
      if (res) {
        console.log('login successfull');
        this.router.navigateByUrl('/home');
      } else {
        this.error = 'login failed';
      }

    }, err => {
      this.error = err;
    });
  }


}
