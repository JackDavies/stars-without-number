import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuardService} from './services/auth/auth-guard.service';
import {HomeComponent} from './pages/home/home.component';
import {LoginComponent} from './pages/login/login.component';
import {CharacterGeneration} from './component/character-generation/character-generation.component';
import {CharactersComponent} from './component/characters/characters.component';
import {JournalsComponent} from './pages/journals/journals.component';
import {FactionsComponent} from './pages/factions/factions.component';
import {NpcComponent} from './pages/npc/npc.component';

const routes: Routes = [
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }, {
    path: 'home',
    component: HomeComponent,
    canActivate: [ AuthGuardService ]
  }, {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Login' },
  }, {
    path: 'character-generation',
    component: CharacterGeneration,
  }, {
    path: 'characters',
    component: CharactersComponent,
    canActivate: [ AuthGuardService ]
  }, {
    path: 'character/:guid',
    component: CharactersComponent,
    canActivate: [ AuthGuardService ]
  }, {
    path: 'journals',
    component: JournalsComponent,
    canActivate: [ AuthGuardService ]
  }, {
    path: 'journals/:guid',
    component: JournalsComponent,
    canActivate: [ AuthGuardService ]
  }, {
    path: 'factions',
    component: FactionsComponent,
    canActivate: [ AuthGuardService ]
  }, {
    path: 'factions/:guid',
    component: FactionsComponent,
    canActivate: [ AuthGuardService ]
  }, {
    path: 'npc',
    component: NpcComponent,
    canActivate: [ AuthGuardService ]
  }, {
    path: 'npc/:guid',
    component: NpcComponent,
    canActivate: [ AuthGuardService ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
  providers: [  ]
})
export class AppRoutingModule {}