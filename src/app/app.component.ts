import {Component, OnInit} from '@angular/core';
import {PlayerService} from './services/player/player.service';
import {AuthService} from './services/auth/auth.service';
import {GameService} from './services/game/game.service';
import {Game} from './definitions/game';
import {Player} from './definitions/player';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  loaded;
  title;
  player;
  game;
  constructor(
    private auth: AuthService,
    private gameService: GameService,
    private playerService: PlayerService
  ) {


  }

  ngOnInit() {
      // GAME
      this.gameService.gameChange.subscribe((game: Game) => {
        console.log(game);
          this.game = game;
          this.title = game.title;
      });

      // PLAYER
      this.player = this.playerService.player;
      this.playerService.playerChange.subscribe((player: Player) => {
          this.player = player;
      });
  }


  public logout() {
    this.auth.logout();
  }
}
